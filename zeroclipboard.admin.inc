<?php
// $Id$

/**
 * @file
 *   Zero Clipboard administration pages.
 */

function zeroclipboard_admin_settings(&$form_state) {
  // Add radio buttons for the actions to take for the listed pages, i.e.
  // disable or enable the zeroclipboard functionality.
  $page_options = array(
    'page_enable' => t('Load only on the listed pages.'),
    'page_disable' => t('Load on every page except the listed pages.'),
  );
  $form['zeroclipboard_page_init_action'] = array(
    '#type' => 'radios',
    '#options' => $page_options,
    '#title' => t('Enable Zero clipboard on specific pages'),
    '#default_value' => variable_get('zeroclipboard_page_init_action', 'page_disable'),
  );
  // Add text input for list of pages to take specific action on.
  $form['zeroclipboard_page_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => t('List one page per line as Drupal paths.  The % character is a wildcard.  Example paths are "node/add/page" and "node/add/%".  Use &lt;front&gt; to match the front page.'),
    '#default_value' => variable_get('zeroclipboard_page_list', ''),
    '#required' => TRUE,
  );
  $form['zeroclipboard_wrapper_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Wrapper ID'),
    '#default_value' => variable_get('zeroclipboard_wrapper_id', ''),
  );
  $form['zeroclipboard_target_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Target ID'),
    '#default_value' => variable_get('zeroclipboard_target_id', ''),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}